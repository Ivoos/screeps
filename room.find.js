var roomNames = ['W1N7'];

Room.prototype.findEnergySources = function(target) { 
	if(this.memory.energySources == undefined) {
		this.memory.energySources = [];
		var sources = this.find(FIND_SOURCES);
		for(var source in sources) {
			this.memory.energySources.push(sources[source].id);
		}
	}

	var objects = [];
	for(var sourceId in this.memory.energySources){
		objects.push(Game.getObjectById(this.memory.energySources[sourceId]));
	}
	return objects;
};

Room.prototype.findEnergyHoldingStructures = function(target) { 
    return this.find(FIND_STRUCTURES, {
        filter: (structure) => {
            return ((structure.structureType == STRUCTURE_EXTENSION ||
                structure.structureType == STRUCTURE_SPAWN ||
                structure.structureType == STRUCTURE_TOWER
                ) && structure.energy < structure.energyCapacity) 
            || (structure.structureType == STRUCTURE_CONTAINER 
            	  && _.sum(structure.store) < structure.storeCapacity);
        }
    });
};

Room.prototype.findContainersWithEnergy = function(target) { 
    return this.find(FIND_STRUCTURES, {
        filter: (structure) => {
            return structure.structureType == STRUCTURE_CONTAINER 
            	  && structure.store[RESOURCE_ENERGY] > 0;
        }
    });
};

Room.prototype.findEnergyHoldingStructuresThatCanSpawn = function(target) { 
    return this.find(FIND_STRUCTURES, {
        filter: (structure) => {
            return (structure.structureType == STRUCTURE_EXTENSION ||
                structure.structureType == STRUCTURE_SPAWN ) 
            && structure.energy < structure.energyCapacity;
        }
    });
};

Room.prototype.findDamagedStructures = function(target) {
	return this.find(FIND_STRUCTURES, {
        filter: (structure) => {
            return (structure.structureType != STRUCTURE_RAMPART && structure.hits < structure.hitsMax / 5) ||
                structure.hits < structure.hitsMax / 50;
        }
    });
};

Room.prototype.findConstructionSites = function(target) {
	return this.find(FIND_MY_CONSTRUCTION_SITES);
};

Room.prototype.findSpawn = function(target) {
	if(this.memory.spawn == undefined) {
		var sources = this.find(FIND_MY_SPAWNS);
		if(sources != undefined && sources.length != 0) {
			this.memory.spawn = sources[0].id;
		}
	}

	return Game.getObjectById(this.memory.spawn);
};








// TODO: refactor!!

Room.prototype.findAllContainers = function(target) { 
	var all;

	for(var index in roomNames) {
		if(Game.rooms[roomNames[index]] != undefined) {
			var structs = Game.rooms[roomNames[index]].findEnergyHoldingStructures();
		}
		if(all == undefined) {
			all = structs;
		} else if(structs != undefined) {
			all.concat(structs);
		}
	}
	if (all == undefined) {
		return '';
	}
	return all;
};

Room.prototype.findAllEnergyHoldingStructuresThatCanSpawn = function(target) { 
	var all;

	for(var index in roomNames) {
		if(Game.rooms[roomNames[index]] != undefined) {
			var structs = Game.rooms[roomNames[index]].findEnergyHoldingStructuresThatCanSpawn();
		}
		if(all == undefined) {
			all = structs;
		} else if(structs != undefined) {
			all.concat(structs);
		}
	}
	if (all == undefined) {
		return '';
	}
	return all;
};

Room.prototype.findAllDamagedStructures = function(target) { 
	var all;

	for(var index in roomNames) {
		if(Game.rooms[roomNames[index]] != undefined) {
			var structs = Game.rooms[roomNames[index]].findDamagedStructures();
		}
		if(all == undefined || all.length == 0) {
			all = structs;
		} else if(structs != undefined) {
			all.concat(structs);
		}
	}
	if (all == undefined) {
		return '';
	}
	return all;
};

Room.prototype.findAllConstructionSites = function(target) { 
	var all;

	for(var index in roomNames) {
		if(Game.rooms[roomNames[index]] != undefined) {
			var structs = Game.rooms[roomNames[index]].findConstructionSites();
		}
		if(all == undefined) {
			all = structs;
		} else if(structs != undefined) {
			all.concat(structs);
		}
	}
	if (all == undefined) {
		return '';
	}
	return all;
};

Room.prototype.findAllSpaws = function(target) { 
	var all;

	for(var index in roomNames) {
		if(Game.rooms[roomNames[index]] != undefined) {
			var structs = Game.rooms[roomNames[index]].findSpawn();
		}
		if(all == undefined) {
			all = structs;
		} else if(structs != undefined) {
			all.concat(structs);
		}
	}
	if (all == undefined) {
		return '';
	}
	return all;
};

Room.prototype.findAllEnergySources = function(target) { 
	var all;

	for(var index in roomNames) {
		if(Game.rooms[roomNames[index]] != undefined) {
			var structs = Game.rooms[roomNames[index]].findEnergySources();
		}
		if(all == undefined) {
			all = structs;
		} else if(structs != undefined) {
			all.concat(structs);
		}
	}
	if (all == undefined) {
		return '';
	}
	return all;
}