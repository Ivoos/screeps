var harvest = require('creep.harvest');
var build = require('creep.build');

var ENUM = require('constants');
var CONSTANTS = new ENUM();

var roleBuilder = {
    run: function(creep) {
    	if(creep.memory.current == CONSTANTS.GATHERING) {
            harvest.run(creep);
        } else if(creep.memory.current == CONSTANTS.RETURNING) {
            build.run(creep);
        }
    }
};

module.exports = roleBuilder;
