var ENUM = require('constants');
var CONSTANTS = new ENUM();

var NUMBER_OF_HARVESTERS = 4;
var NUMBER_OF_UPGRADERS = 6;
var NUMBER_OF_BUILDERS = 4;
var NUMBER_OF_CLAIMERS = 0;
var NUMBER_OF_MINERS = 0;

var spawn = {
    run: function(spawner) {  
        // Try to prevent unneeded rounds trips for creeps
        tryToRenewBots(spawner);

        tryToSpawnBots(spawner);
    }
}

var tryToRenewBots = function (spawner) {
    var creepsCloseBy = spawner.pos.findInRange(FIND_MY_CREEPS, 1);
    if(creepsCloseBy.length > 0) {
        for(var creep in creepsCloseBy) {
            if(creepsCloseBy[creep].ticksToLive < parseInt(CONSTANTS.MINTICKSTOLIVE) + 200) {
                spawner.renewCreep(creepsCloseBy[creep]);
            }
        }
    }
}

var tryToSpawnBots = function (spawner) {
    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

    var claimers = _.filter(Game.creeps, (creep) => creep.memory.role == CONSTANTS.CLAIMER);
    var harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == CONSTANTS.HARVESTER);
    var upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == CONSTANTS.UPGRADER);
    var builders = _.filter(Game.creeps, (creep) => creep.memory.role == CONSTANTS.BUILDER);
    var miners = _.filter(Game.creeps, (creep) => creep.memory.role == CONSTANTS.MINER);

    if(claimers.length < NUMBER_OF_CLAIMERS) {
        tryToSpawnSingleBot(CONSTANTS.CLAIMER, spawner);
    } else if(harvesters.length < NUMBER_OF_HARVESTERS) {
        tryToSpawnSingleBot(CONSTANTS.HARVESTER, spawner);
    }
    else if(upgraders.length < NUMBER_OF_UPGRADERS) {
        tryToSpawnSingleBot(CONSTANTS.UPGRADER, spawner);
    }
    else if(builders.length < NUMBER_OF_BUILDERS) {
        tryToSpawnSingleBot(CONSTANTS.BUILDER, spawner);
    }
    else if(miners.length < NUMBER_OF_MINERS) {
        tryToSpawnSingleBot(CONSTANTS.MINER, spawner);
    }

    if(spawner.spawning) {
        var spawningCreep = Game.creeps[spawner.spawning.name];
        spawner.room.visual.text(
            'Spawning ' + spawningCreep.memory.role,
            spawner.pos.x + 1,
            spawner.pos.y,
            {align: 'left', opacity: 0.8});
    }
}

var tryToSpawnSingleBot = function (role, spawner) {
    if(spawner.spawning) {
        return;
    }

    var time = Game.time;
    var newName = role + time;
    var body;

    if(role == CONSTANTS.CLAIMER){ 
        body = [CLAIM,MOVE,MOVE];
    } else if (role == CONSTANTS.MINER) {
        body = [WORK,WORK,WORK,MOVE,MOVE];
    } else {
        if(Game.creeps == undefined || Object.keys(Game.creeps).length < 12) {
            body = [WORK,CARRY,MOVE,MOVE];
        } else {
            body = [WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE];
        }
    }
    if(spawner.spawnCreep(body, newName,
        {memory: {role: role, rand: time, current: CONSTANTS.GATHERING}}) == 0) {
        console.log('Spawning new ' + role + ' : ' + newName);
    }
}

module.exports = spawn;