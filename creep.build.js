var upgrade = require('creep.upgrade');

var ENUM = require('constants');
var CONSTANTS = new ENUM();

var build = {

    /** @param {Creep} creep **/
    run: function(creep) {
        // damaged structures in this room
        var damagedStructure = creep.findClosestDamagedStructure();
        if(damagedStructure != undefined) {
            creep.moveToTarget(damagedStructure);
            creep.repair(damagedStructure);
        } else {
            // damaged structures in all my rooms
            var damagedStructureAllRooms = creep.room.findAllDamagedStructures();
            if(damagedStructureAllRooms.length != 0) {
                creep.moveToTarget(damagedStructureAllRooms[0]);
                creep.repair(damagedStructureAllRooms[0]);
            } else {
                // construction sites in this room
                var constructionSite = creep.findClosestConstructionSite();
                if(constructionSite != undefined) {
                    creep.moveToTarget(constructionSite);
                    creep.build(constructionSite);
                } else {
                // construction sites in all rooms
                    constructionSites = creep.room.findAllConstructionSites();
                    if(constructionSites.length != 0) {
                        creep.moveToTarget(constructionSites[0]);
                        creep.build(constructionSites[0]);
                    } else {
                        upgrade.run(creep);
                    }
                }
            }
        }
        if(creep.carry.energy == 0){
            creep.memory.current = CONSTANTS.GATHERING;
        }

    }
}

module.exports = build;
