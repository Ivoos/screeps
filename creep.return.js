var build = require('creep.build');

var ENUM = require('constants');
var CONSTANTS = new ENUM();

var returning = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.carry.energy == 0) {
            creep.memory.current = CONSTANTS.GATHERING;
        } else {
            var targets = creep.room.findEnergyHoldingStructuresThatCanSpawn();
            if(targets.length != 0) {
                Memory.spawnEnergyFull = false;
                if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveToTarget(targets[0]);
                }
            } else {
                targets = creep.room.findEnergyHoldingStructures();
                if(targets.length != 0) {
                    Memory.spawnEnergyFull = true;
                    if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveToTarget(targets[0]);
                    }
                }  else {
                    targets = creep.room.findAllEnergyHoldingStructuresThatCanSpawn();
                    if(targets.length != 0) {
                        Memory.spawnEnergyFull = true;
                        if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                            creep.moveToTarget(targets[0]);
                        }
                    } else {
                        targets = creep.room.findAllContainers();
                        if(targets.length != 0) {
                            Memory.spawnEnergyFull = true;
                            if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                                creep.moveToTarget(targets[0]);
                            }
                        }  else {
                            build.run(creep);
                        }
                    }
                }
            }
        }
    }
};

module.exports = returning;
