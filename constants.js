let CONSTANTS = function () {
	// gatherer states
	this.GATHERING = 'GATHERING';
	this.RETURNING = 'RETURNING';
	this.RENEWING = 'RENEWING';

	// roles
	this.HARVESTER = 'HARVESTER';
	this.MINER = 'MINER';
	this.UPGRADER = 'UPGRADER';
	this.BUILDER = 'BUILDER';
	this.CLAIMER = 'CLAIMER';

	this.MINTICKSTOLIVE = '250';
}

module.exports = CONSTANTS;