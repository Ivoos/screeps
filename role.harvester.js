var harvest = require('creep.harvest');
var returning = require('creep.return');

var ENUM = require('constants');
var CONSTANTS = new ENUM();

var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.memory.current == CONSTANTS.GATHERING) {
            harvest.run(creep);
        } else if(creep.memory.current == CONSTANTS.RETURNING) {
            returning.run(creep);
        } 
    }
};

module.exports = roleHarvester;
