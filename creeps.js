var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleClaimer = require('creep.claim');
var renew = require('creep.renew');

var ENUM = require('constants');
var CONSTANTS = new ENUM();

var creeps = {

    run: function() {
        var indexedListOfCreeps = [];

        for(var name in Game.creeps) {
            var creep = {};
            creep['name'] = name;
            creep['object'] = Game.creeps[name];

            indexedListOfCreeps.push(creep);
        }

        var index = Game.time % Object.keys(Game.creeps).length;

        var virtualNumberOfActions = 0;

        while((Game.cpu.getUsed() < Game.cpu.limit - 5 || virtualNumberOfActions < Object.keys(Game.creeps).length -1)
                    // All creeps should have moved if we do number of creeps² runs
                    && virtualNumberOfActions < Object.keys(Game.creeps).length * Object.keys(Game.creeps).length) {
            var creep = indexedListOfCreeps[index];
            if(creep.object.memory.fatigue > 0) {
                continue;
            }
            if(creep.object.body.length > 6 && (creep.object.ticksToLive < parseInt(CONSTANTS.MINTICKSTOLIVE) || creep.object.memory.current == CONSTANTS.RENEWING)) {
                renew.run(creep.object);
            } else if(creep.object.memory.role == CONSTANTS.HARVESTER) {
                roleHarvester.run(creep.object);
            } else if(creep.object.memory.role == CONSTANTS.UPGRADER) {
                roleUpgrader.run(creep.object);
            } else if(creep.object.memory.role == CONSTANTS.BUILDER) {
                roleBuilder.run(creep.object);
            } else if(creep.object.memory.role == CONSTANTS.CLAIMER) {
                roleClaimer.run(creep.object);
            }
            index++; 
            index = index %Object.keys(Game.creeps).length;
            virtualNumberOfActions++;
        }

        if(Game.cpu.getUsed() > Game.cpu.limit) {
            console.log('WARNING!! Too much CPU used!! Lost ' + (Game.cpu.getUsed() - Game.cpu.limit) + ' CPU from the bucket');
        }
    }
}

module.exports = creeps;
