var ENUM = require('constants');
var CONSTANTS = new ENUM();

var renew = {
    run: function(creep) {
        creep.memory.current = CONSTANTS.RENEWING;
        
        var spawn = creep.room.findSpawn();
        if(spawn != undefined) {
            creep.moveToTarget(spawn);
            creep.transfer(spawn, RESOURCE_ENERGY);
        } else {
            spawns = creep.room.findAllSpawns();
            if(spawns.length != 0) {
                creep.moveToTarget(spawns[0]);
                creep.transfer(spawns[0], RESOURCE_ENERGY);
            }
        }
        if(creep.ticksToLive > parseInt(CONSTANTS.MINTICKSTOLIVE) + 200) {
            creep.memory.current = CONSTANTS.GATHERING;
        }
    }
}

module.exports = renew;
