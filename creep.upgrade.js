var ENUM = require('constants');
var CONSTANTS = new ENUM();

var upgrade = {

    /** @param {Creep} creep **/
    run: function(creep) {
        creep.upgradeController(creep.room.controller);
        // move closer to make room for more creeps around it
        creep.moveToTarget(creep.room.controller);
        
        if(creep.carry.energy == 0) {
            creep.memory.current = CONSTANTS.GATHERING;
        }
    }
};

module.exports = upgrade;
