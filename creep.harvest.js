var ENUM = require('constants');
var CONSTANTS = new ENUM();

var harvest = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.carry.energy < creep.carryCapacity) {
            var sources = creep.room.findAllEnergySources();
            if (creep.memory.role == CONSTANTS.BUILDER) {
                var containers = creep.room.findContainersWithEnergy();
                if(containers.length != 0 && 
                    (creep.room.findPath(creep.pos, containers[0].pos) < creep.room.findPath(creep.pos, sources[1].pos) || sources[1].energy == 0)
                    ) {
                    withdrawFromContainer(containers[0], creep);
                } else {
                    move(creep, sources[1]);
                }
            } else if (creep.memory.role == CONSTANTS.HARVESTER) {
                if(Memory.spawnEnergyFull == false) {
                    var containers = creep.room.findContainersWithEnergy();
                    if(containers.length != 0) {
                        withdrawFromContainer(containers[0], creep);
                    } else {
                        move(creep, sources[0]);
                    }
                } else {
                    move(creep, sources[0]);
                }
            } else if (creep.memory.role == CONSTANTS.UPGRADER) {
                move(creep, sources[1]);
            }
        } else {
            creep.memory.current = CONSTANTS.RETURNING;
        }
    }
};

module.exports = harvest;

var move = function(creep, source) {
    if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
        creep.moveToTarget(source);
    }
}

var withdrawFromContainer = function(container, creep) {
    creep.moveToTarget(container);
    creep.withdraw(container, RESOURCE_ENERGY);
    // Don't go back to the mine if containers are almost empty
    if(creep.carry.energy != 0) {
        creep.memory.current = CONSTANTS.RETURNING;
    }
}