var harvest = require('creep.harvest');
var upgrade = require('creep.upgrade');

var ENUM = require('constants');
var CONSTANTS = new ENUM();

var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.memory.current == CONSTANTS.GATHERING) {
            harvest.run(creep);
        } else if(creep.memory.current == CONSTANTS.RETURNING){
            upgrade.run(creep);
        }
    }
};

module.exports = roleUpgrader;
