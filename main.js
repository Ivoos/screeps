//Prototype imports
var move = require('creep.move');
var roomFind = require('room.find');
var creepFind = require('creep.find');


var creeps = require('creeps');
var spawn = require('spawn');

var ENUM = require('constants');
var CONSTANTS = new ENUM();

module.exports.loop = function () {
    spawn.run(Game.spawns['spawner']);
    creeps.run();
}