Creep.prototype.findClosestDamagedStructure = function() {
	return this.pos.findClosestByPath(FIND_STRUCTURES, {
        filter: (structure) => {
            return (structure.structureType != STRUCTURE_RAMPART && structure.hits < structure.hitsMax / 5) ||
                structure.hits < structure.hitsMax / 50;
        }
    });
};

Creep.prototype.findClosestConstructionSite = function() {
	return this.pos.findClosestByPath(FIND_MY_CONSTRUCTION_SITES);
};